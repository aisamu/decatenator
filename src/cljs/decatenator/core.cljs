(ns decatenator.core
  (:require [clojure.string :as string]
            [devtools.core :as devtools]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [reagent.core :as reagent]
            [secretary.core :as secretary])
  (:require-macros [secretary.core :refer [defroute]])
  (:import goog.History))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Vars

(defonce debug?
  ^boolean js/goog.DEBUG)

(defn largest-string-comparator [s1 s2]
  (let [comp-vec (juxt count identity)]
    (compare (comp-vec s2) (comp-vec s1))))

(defonce app-state
  (reagent/atom
   {:input-text  "I reset the computer. It still didn't boot!"
    :stripped-text "iresetthecomputeritstilldidntboot"
    :token-set (sorted-set-by largest-string-comparator "set" "reset" "computer" "still")
    :parsing-result []
    :text "Hello, what is your name? "
    :page :nil}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Next token logic

(declare get-all-tokens)

(defn get-next-token [[token-set unparsed-string parsed-string]]
  (if (empty? unparsed-string)
    [token-set unparsed-string parsed-string]

    (let [largest-token-size (-> token-set first count)
          consumable-string-length (min (count unparsed-string ) (* 2 largest-token-size))
          parse-target (subs unparsed-string 0 consumable-string-length)
          regex (re-pattern (str "^(.*?)(" (clojure.string/join "|" (:token-set @app-state)) ")(.*?)$"))
          [_ pre match post] (first (re-seq regex parse-target))]

      (if (empty? match)
        [token-set (subs unparsed-string 1) (conj parsed-string [:? (first parse-target)])]

        (let [[_ _ pre-parsed-string] (get-all-tokens [token-set pre parsed-string])
              consumed-string-length (if (nil? match) consumable-string-length (+ (count pre) (count match)))]
          [token-set (subs unparsed-string consumed-string-length) (conj pre-parsed-string match)])))))

(defn get-all-tokens [[token-set unparsed-string parsed-string]]
  (->> (iterate get-next-token [token-set unparsed-string parsed-string])
       (drop-while #(not (empty? (second %))))
       first))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Routes

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")

  (defroute "/" []
    (swap! app-state assoc :page :home))

  (defroute "/about" []
    (swap! app-state assoc :page :about))

  ;; add routes here


  (hook-browser-navigation!))

(defn concatenate-strip-string [string]
  (string/lower-case (string/replace string #"[^a-zA-Z0-9]" "")))

(defn update-input-string [ratom string]
  (reset! ratom (assoc @ratom
                       :input-text string
                       :stripped-text (concatenate-strip-string string))))

(defn update-dictionary [ratom string]
  (reset! ratom (assoc @ratom
                       :token-set (into (sorted-set) (filter (complement empty?) (string/split string #"[^\w|-]"))))))

;; (into (sorted-set) (filter (complement empty?) (string/split "ad-hoc,,,,,,computer,still,reset,set" #"[^\w|-]")))


(defn decatenation-iteration [[token-set unparsed-string parsed-string]]
  [:div
   #_[:span (str token-set)]
   [:span (str parsed-string)]
   [:span (str unparsed-string)]])


(defn decatenation-steps [ratom]
  (let [should-start (reagent/atom false)]
    (fn []
      [:div
       [:button {:on-click #(reset! should-start true)} "Compute with steps"]
       (when @should-start
         (for [iter (doall (take-while
                            #(not (empty? (second %)))
                            (iterate get-next-token [(:token-set @ratom) (:stripped-text @ratom) []])))]
           ^{:key (second iter)} [decatenation-iteration iter]))])))

(defn format-result-string [result]
  (let [pred #(= (first %) :?)
        groups (partition-by pred result)
        to-uppercase (fn [v] (if (pred v) (string/upper-case (second v)) v))
        uppercased (map #(map to-uppercase %) groups)]
    (->> (map #(string/join "" %) uppercased)
         (string/join " "))))

(defn decatenation [ratom]
  (let [should-start (reagent/atom false)]
    (fn []
      [:div
       [:button {:on-click #(reset! should-start true)} "Compute result"]
       (when @should-start
         (let [[_ _ result] (get-all-tokens [(:token-set @ratom) (:stripped-text @ratom) []])]
           [:p (format-result-string result)]))])))

 ;;[decatenation-iteration result]

(defn dictionary-section [ratom]
  (let [dict-string (reagent/atom (:token-set @ratom))]
    (fn []
      [:div
       [:label "Dictionary (whitespace/comma separated): "]
       [:input {:type "text"
                :value @dict-string
                :on-change #(reset! dict-string (-> % .-target .-value))
                :on-blur #(update-dictionary ratom @dict-string)}]
       [:p (:token-set @ratom)]])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pages

(defn home [ratom]
  [:div
   [:h1 "Decatenator"]
   [:div
    [:label "Original/corrupted string: "]
    [:input {:type "text"
             :value (:input-text @ratom)
             :on-change #(update-input-string ratom (-> % .-target .-value))}]]
   [:div
    [:label "Working string: "]
    [:span {:style {:color 'red}} (:stripped-text @ratom)]]

   [dictionary-section ratom]
   [decatenation ratom]
   [decatenation-steps ratom]
   ]
  )


(defn old-home [ratom]
  (let [text (:text @ratom)]
    [:div [:h1 "Home Page"]
     [:p text "FIXME"]
     [:a {:href "#/about"} "about page"]]))

(defn about [ratom]
  [:div [:h1 "About Page"]
   [:a {:href "#/"} "home page"]])



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Initialize App

(defmulti page identity)
(defmethod page :home [] home)
(defmethod page :about [] about)
(defmethod page :default [] (fn [_] [:div]))

(defn current-page [ratom]
  (let [page-key (:page @ratom)]
    [(page page-key) ratom]))

(defn dev-setup []
  (when debug?
    (enable-console-print!)
    (println "dev mode")
    (devtools/install!)
    ))

(defn reload []
  (reagent/render [current-page app-state]
                  (.getElementById js/document "app")))

(defn ^:export main []
  (dev-setup)
  (app-routes)
  (reload))
